package org.bapcraft.particles.persist;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class FileUserStorage implements UserStorage {

	private File userDir;
	private Logger logger;

	public FileUserStorage(File dir, Logger log) {
		this.userDir = dir;
		this.logger = log;
	}

	@Override
	public void setPlayerProfile(UUID uuid, ParticleProfile prof) {

		File userFile = this.getUserFile(uuid);

		if (!userFile.exists()) {
			try {
				userFile.getParentFile().mkdirs();
				userFile.createNewFile();
			} catch (IOException e) {
				this.logger.error("Couldn't create user file for user " + uuid + "!", e);
			}
		}

		try (FileWriter fw = new FileWriter(userFile)) {
			this.makeGson().toJson(prof, fw);
		} catch (IOException e) {
			this.logger.error("Couldn't write player data for user " + uuid + "!");
		}

	}

	@Override
	public void deletePlayerProfile(UUID uuid) {
		this.getUserFile(uuid).delete();
	}

	@Override
	public Optional<ParticleProfile> getPlayerProfile(UUID uuid) {

		File userFile = this.getUserFile(uuid);
		if (!userFile.exists()) {
			return Optional.empty();
		}

		try {
			return Optional.of(this.makeGson().fromJson(new FileReader(userFile), ParticleProfile.class));
		} catch (JsonSyntaxException | JsonIOException | FileNotFoundException e) {
			userFile.delete();
			this.logger.warn("Error reading user data file for " + uuid + "!");
			return null;
		}

	}

	private File getUserFile(UUID uuid) {
		return new File(this.userDir, uuid.toString() + ".json");
	}

	private Gson makeGson() {
		return new GsonBuilder().setLenient().serializeNulls().setPrettyPrinting().create();
	}

}
