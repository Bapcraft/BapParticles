package org.bapcraft.particles.persist;

import java.util.Optional;
import java.util.UUID;

public interface UserStorage {

	void setPlayerProfile(UUID uuid, ParticleProfile prof);

	void deletePlayerProfile(UUID uuid);

	Optional<ParticleProfile> getPlayerProfile(UUID uuid);

}
