package org.bapcraft.particles;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import org.bapcraft.particles.cmd.ClearParticleExecutor;
import org.bapcraft.particles.cmd.ListParticlesExecutor;
import org.bapcraft.particles.cmd.SetParticleExecutor;
import org.bapcraft.particles.persist.FileUserStorage;
import org.bapcraft.particles.persist.UserStorage;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.asset.Asset;
import org.spongepowered.api.asset.AssetManager;
import org.spongepowered.api.command.CommandMapping;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;

import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;

import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;

@Plugin(id = "bapparticles", name = "BapParticles", version = "0.1")
public class BapParticlesPlugin {

	@Inject
	private Logger logger;

	private Path configPath;

	@Inject
	@DefaultConfig(sharedRoot = true)
	private ConfigurationLoader<CommentedConfigurationNode> configLoader;

	private BPConfig config;
	private UserStorage storage;

	private EffectManager effectMan;

	@Inject
	public BapParticlesPlugin(@DefaultConfig(sharedRoot = true) final Path configPath) {
		this.configPath = configPath;
	}

	@Listener
	public void onInit(GameInitializationEvent event) throws Exception {

		// Load config.
		AssetManager assets = Sponge.getAssetManager();
		Asset cfgAsset = assets.getAsset(this, "default.conf").get();
		try {

			File configFile = this.configPath.toFile();
			if (!configFile.exists()) {
				cfgAsset.copyToFile(this.configPath);
			}

			ConfigurationNode root = this.configLoader.load();
			this.config = root.getValue(TypeToken.of(BPConfig.class));

		} catch (IOException e) {
			this.logger.error("Unable to load config!");
			throw e;
		} catch (ObjectMappingException e) {
			this.logger.error("Unable to parse config!");
			throw e;
		}

		// Prepare storage backend, just always using plain files for now.
		File storageDir = new File("bapparticles");
		if (!(storageDir.exists() && storageDir.isDirectory())) {
			storageDir.mkdirs();
		}

		this.storage = new FileUserStorage(new File(storageDir, "users"), this.logger);
		this.effectMan = new EffectManager(this.storage, this.logger);

		// Start the task to actually spawn the particle effects.
		Sponge.getScheduler().createTaskBuilder()
				.name("Player Particle Spawn Task")
				.intervalTicks(1)
				.execute(new ParticleSpawnTask(this.effectMan))
				.submit(this);

		// Set up commands.
		CommandSpec ppSetCmd = CommandSpec.builder()
				.description(Text.of("Set your particle effect"))
				.permission("bapparticles.cmd.particle.set")
				.arguments(GenericArguments.string(Text.of("effectname")))
				.executor(new SetParticleExecutor(this.effectMan)).build();

		CommandSpec ppClearCmd = CommandSpec.builder()
				.description(Text.of("Clear particle effect."))
				.permission("bapparticles.cmd.particle.clear")
				.executor(new ClearParticleExecutor(this.effectMan))
				.build();

		CommandSpec ppLsCmd = CommandSpec.builder()
				.description(Text.of("List particle effect names."))
				.permission("bapparticles.cmd.particle.list")
				.executor(new ListParticlesExecutor())
				.build();

		CommandSpec ppCmd = CommandSpec.builder()
				.child(ppSetCmd, "set")
				.child(ppClearCmd, "clear")
				.child(ppLsCmd, "list", "ls")
				.build();

		CommandMapping m = Sponge.getCommandManager().register(this, ppCmd, "bapparticles", "particles", "bp").get();
		this.logger.info("Registered command: " + m.getPrimaryAlias());

	}

}
