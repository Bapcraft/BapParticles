package org.bapcraft.particles.effects;

import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

public interface Effect {

	int getInterval();

	void apply(Location<World> playerLocation);

}
