package org.bapcraft.particles.effects;

import org.spongepowered.api.effect.particle.ParticleEffect;
import org.spongepowered.api.effect.particle.ParticleTypes;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;

public class DebugEffect implements Effect {

	@Override
	public void apply(Location<World> loc) {

		World w = loc.getExtent();
		ParticleEffect pe = ParticleEffect.builder()
				.offset(Vector3d.UNIT_Y)
				.type(ParticleTypes.SNOW_SHOVEL)
				.build();

		for (int i = 0; i < 10; i++) {
			w.spawnParticles(pe, loc.getPosition());
		}

	}

	@Override
	public int getInterval() {
		return 1;
	}

}
