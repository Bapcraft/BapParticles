package org.bapcraft.particles.effects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.spongepowered.api.effect.particle.ParticleTypes;

public class Effects {

	private static Map<String, Effect> effects = new HashMap<>();

	public static final Effect DEBUG = new DebugEffect();

	public static final Effect FLAME_AURA = new AuraEffect(ParticleTypes.FLAME, 2D, 0.15D);
	public static final Effect EXPLOSION_AURA = new AuraEffect(ParticleTypes.EXPLOSION, 5D, 0.015D);
	public static final Effect SMOKE_AURA = new AuraEffect(ParticleTypes.SMOKE, 3D, 0.1D);
	public static final Effect SPLASH_AURA = new AuraEffect(ParticleTypes.WATER_SPLASH, 3D, 0.25D);
	public static final Effect DRAGON_BREATH_AURA = new AuraEffect(ParticleTypes.DRAGON_BREATH, 3D, 0.1D);
	public static final Effect HAPPY_VILLAGER_AURA = new AuraEffect(ParticleTypes.HAPPY_VILLAGER, 2D, 0.2D);

	public static final Effect GLYPHS_SHIMMER = new ShimmerEffect(ParticleTypes.ENCHANTING_GLYPHS, 3);
	public static final Effect WATER_SHIMMER = new ShimmerEffect(ParticleTypes.DRIP_WATER, 1);
	public static final Effect LAVA_SHIMMER = new ShimmerEffect(ParticleTypes.LAVA, 1);
	public static final Effect FIRE_SHIMMER = new ShimmerEffect(ParticleTypes.FLAME, 2);
	public static final Effect GROW_SHIMMER = new ShimmerEffect(ParticleTypes.FERTILIZER, 1);
	public static final Effect SLIME_SHIMMER = new ShimmerEffect(ParticleTypes.SLIME, 2);
	public static final Effect PORTAL_SHIMMER = new ShimmerEffect(ParticleTypes.PORTAL, 3);

	public static final Effect FLAME_BURST = new BurstEffect(ParticleTypes.FLAME, 5, 0.15D);
	public static final Effect SPELL_BURST = new BurstEffect(ParticleTypes.MOB_SPELL, 3, 0.2D);
	public static final Effect DRAGON_BREATH_BURST = new BurstEffect(ParticleTypes.DRAGON_BREATH, 3, 0.1D);

	public static Effect getByName(String name) {
		return effects.get(name);
	}

	public static String getEffectName(Effect effect) {
		for (Map.Entry<String, Effect> e : effects.entrySet()) {
			if (e.getValue() == effect) {
				return e.getKey();
			}
		}
		return null;
	}

	public static List<String> getEffectNames() {
		return new ArrayList<String>(effects.keySet());
	}

	static {

		effects.put("debug", DEBUG);

		effects.put("flame_aura", FLAME_AURA);
		effects.put("explosion_aura", EXPLOSION_AURA);
		effects.put("smoke_aura", SMOKE_AURA);
		effects.put("splash_aura", SPLASH_AURA);
		effects.put("dragonbreath_aura", DRAGON_BREATH_AURA);
		effects.put("happyvillager_aura", HAPPY_VILLAGER_AURA);

		effects.put("glyph_shimmer", GLYPHS_SHIMMER);
		effects.put("water_shimmer", WATER_SHIMMER);
		effects.put("lava_shimmer", LAVA_SHIMMER);
		effects.put("fire_shimmer", FIRE_SHIMMER);
		effects.put("grow_shimmer", GROW_SHIMMER);
		effects.put("slime_shimmer", SLIME_SHIMMER);
		effects.put("portal_shimmer", PORTAL_SHIMMER);

		effects.put("flame_burst", FLAME_BURST);
		effects.put("spell_burst", SPELL_BURST);
		effects.put("dragonbreath_burst", DRAGON_BREATH_BURST);

	}

}
