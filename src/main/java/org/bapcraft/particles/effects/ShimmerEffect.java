package org.bapcraft.particles.effects;

import java.util.Random;

import org.spongepowered.api.effect.particle.ParticleEffect;
import org.spongepowered.api.effect.particle.ParticleType;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;

public class ShimmerEffect implements Effect {

	private ParticleType type;
	private int rounds;

	public ShimmerEffect(ParticleType type, int rounds) {
		this.type = type;
		this.rounds = rounds;
	}

	@Override
	public int getInterval() {
		return 3;
	}

	@Override
	public void apply(Location<World> loc) {

		Random r = new Random();
		for (int i = 0; i < this.rounds; i++) {

			// This is just simple math.
			double x = r.nextDouble() * 1.5D - 0.75D;
			double y = r.nextDouble() * 2;
			double z = r.nextDouble() * 1.5D - 0.75D;
			Vector3d pos = loc.getPosition()
					.add(Vector3d.UNIT_X.mul(x))
					.add(Vector3d.UNIT_Y.mul(y))
					.add(Vector3d.UNIT_Z.mul(z));

			ParticleEffect pe = ParticleEffect.builder()
					.type(this.type)
					.build();

			loc.getExtent().spawnParticles(pe, pos);
		}

	}

}
