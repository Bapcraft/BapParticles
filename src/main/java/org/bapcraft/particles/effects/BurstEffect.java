package org.bapcraft.particles.effects;

import java.util.Random;

import org.spongepowered.api.effect.particle.ParticleEffect;
import org.spongepowered.api.effect.particle.ParticleType;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;

public class BurstEffect implements Effect {

	private ParticleType type;
	private int rounds;
	double maxVelocity;

	public BurstEffect(ParticleType type, int rounds, double maxVel) {
		this.type = type;
		this.rounds = rounds;
		this.maxVelocity = maxVel;
	}

	@Override
	public int getInterval() {
		return 2;
	}

	@Override
	public void apply(Location<World> loc) {

		Random r = new Random();

		for (int i = 0; i < this.rounds; i++) {

			double theta = r.nextFloat() * 2 * Math.PI;
			double xVel = this.maxVelocity * Math.cos(theta);
			double zVel = this.maxVelocity * Math.sin(theta);
			double xStart = Math.cos(theta);
			double zStart = Math.sin(theta);

			Vector3d vel = Vector3d.UNIT_X.mul(xVel).add(Vector3d.UNIT_Z.mul(zVel));
			Vector3d off = Vector3d.UP.mul(r.nextDouble() * 2)
					.add(Vector3d.UNIT_X.mul(xStart))
					.add(Vector3d.UNIT_Z.mul(zStart));

			ParticleEffect pe = ParticleEffect.builder()
					.velocity(vel)
					.type(this.type)
					.build();

			loc.getExtent().spawnParticles(pe, loc.getPosition().add(off));

		}

	}

}
