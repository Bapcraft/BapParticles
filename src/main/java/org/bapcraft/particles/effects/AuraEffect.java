package org.bapcraft.particles.effects;

import java.util.Random;

import org.spongepowered.api.effect.particle.ParticleEffect;
import org.spongepowered.api.effect.particle.ParticleType;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import com.flowpowered.math.vector.Vector3d;

public class AuraEffect implements Effect {

	private ParticleType type;

	private double radius;
	private int rounds;

	public AuraEffect(ParticleType type, double radius, double densityCoeff) {

		this.type = type;

		this.radius = radius;
		this.rounds = (int) (Math.pow(radius, 3) * densityCoeff);
		System.out.println("num rounds for " + type.getName() + " aura effect is " + this.rounds);

	}

	@Override
	public int getInterval() {
		return 2;
	}

	@Override
	public void apply(Location<World> loc) {

		World w = loc.getExtent();
		Random r = new Random();

		for (int i = 0; i < this.rounds; i++) {

			ParticleEffect pe = ParticleEffect.builder()
					.type(this.type)
					.build();

			double factor = Math.pow(r.nextDouble(), 1D / 3D) * this.radius;
			Vector3d pos = Vector3d.createRandomDirection(r).mul(factor);
			Vector3d off = Vector3d.UP.mul(this.radius / 2);
			w.spawnParticles(pe, loc.getPosition().add(pos).add(off));

		}

	}

}
