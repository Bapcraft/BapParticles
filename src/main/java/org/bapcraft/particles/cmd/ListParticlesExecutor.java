package org.bapcraft.particles.cmd;

import java.util.Collections;
import java.util.List;

import org.bapcraft.particles.effects.Effects;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.text.Text;

public class ListParticlesExecutor implements CommandExecutor {

	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

		src.sendMessage(Text.of("Particle effects:"));
		List<String> names = Effects.getEffectNames();
		Collections.sort(names);
		for (String name : names) {
			src.sendMessage(Text.of(" - " + name));
		}

		return CommandResult.success();

	}

}
