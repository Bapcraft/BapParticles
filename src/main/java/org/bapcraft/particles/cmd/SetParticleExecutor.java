package org.bapcraft.particles.cmd;

import org.bapcraft.particles.EffectManager;
import org.bapcraft.particles.effects.Effect;
import org.bapcraft.particles.effects.Effects;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class SetParticleExecutor implements CommandExecutor {

	private EffectManager effectMan;

	public SetParticleExecutor(EffectManager man) {
		this.effectMan = man;
	}

	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

		if (src instanceof Player) {

			String effectName = (String) args.getOne("effectname").get();

			Player p = (Player) src;

			if (!p.hasPermission("bapparticles.effect." + effectName)) {
				p.sendMessage(Text.of("You don't have the permission to use this particle effect."));
				return CommandResult.empty();
			}

			Effect e = Effects.getByName(effectName);
			if (e == null) {
				p.sendMessage(Text.of("Effect name not found."));
				return CommandResult.empty();
			}

			this.effectMan.setPlayerEffect(p, e);

			p.sendMessage(Text.of("Effect set."));

			return CommandResult.success();

		} else {
			src.sendMessage(Text.of("You must be a player to have particle effects."));
			return CommandResult.empty();
		}

	}

}
