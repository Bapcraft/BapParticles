package org.bapcraft.particles.cmd;

import org.bapcraft.particles.EffectManager;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class ClearParticleExecutor implements CommandExecutor {

	private EffectManager effectMan;

	public ClearParticleExecutor(EffectManager man) {
		this.effectMan = man;
	}

	@Override
	public CommandResult execute(CommandSource src, CommandContext args) throws CommandException {

		if (src instanceof Player) {

			// This is really simple.
			Player p = (Player) src;
			this.effectMan.removePlayerEffect(p);
			p.sendMessage(Text.of("Effect cleared."));
			return CommandResult.success();

		} else {
			src.sendMessage(Text.of("You must be a player to have particle effects."));
			return CommandResult.empty();
		}

	}

}
