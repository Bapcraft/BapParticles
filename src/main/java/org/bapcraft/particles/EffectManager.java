package org.bapcraft.particles;

import java.util.Optional;
import java.util.WeakHashMap;

import org.bapcraft.particles.effects.Effect;
import org.bapcraft.particles.effects.Effects;
import org.bapcraft.particles.persist.ParticleProfile;
import org.bapcraft.particles.persist.UserStorage;
import org.slf4j.Logger;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;

public class EffectManager {

	private UserStorage storage;
	private Logger logger;

	private WeakHashMap<Player, ParticleProfile> profCache = new WeakHashMap<>();
	private WeakHashMap<Player, Effect> effectCache = new WeakHashMap<>();

	public EffectManager(UserStorage storage, Logger log) {
		this.storage = storage;
		this.logger = log;
	}

	public Optional<Effect> getPlayerEffect(Player p) {

		// We use separate caches here because if a player doesn't have an effect set,
		// they still have a profile, usually.
		if (this.profCache.containsKey(p)) {
			return Optional.ofNullable(this.effectCache.get(p));
		} else {
			return this.loadEffectNow(p);
		}

	}

	public Optional<Effect> loadEffectNow(Player p) {

		// Remove the old data.
		this.profCache.remove(p);
		this.effectCache.remove(p);

		Optional<ParticleProfile> ppo = this.storage.getPlayerProfile(p.getUniqueId());
		if (!ppo.isPresent()) {
			return Optional.empty();
		}

		ParticleProfile pp = ppo.get();
		this.profCache.put(p, pp);
		Effect e = Effects.getByName(pp.name);

		if (e == null) {
			this.logger.warn("Unknown effect " + pp.name + " from loaded player data in " + p.getUniqueId());
			this.storage.deletePlayerProfile(p.getUniqueId());
			p.sendMessage(Text.of(
					"Something went weird when loading your particle effect data, we've deleted it so try setting a new effect."));
			return Optional.empty();
		}

		// Now store the data in cache and return.
		this.effectCache.put(p, e);
		return Optional.of(e);

	}

	public void setPlayerEffect(Player p, Effect e) {

		// Just create the new profile...
		ParticleProfile pp = new ParticleProfile();
		pp.name = Effects.getEffectName(e);

		// ...and save it in all the relevant locations.
		this.profCache.put(p, pp);
		this.effectCache.put(p, e);
		this.storage.setPlayerProfile(p.getUniqueId(), pp);

	}

	public void removePlayerEffect(Player p) {
		this.storage.deletePlayerProfile(p.getUniqueId());
		this.clearCache(p);
	}

	public void clearCache(Player p) {
		this.profCache.remove(p);
		this.effectCache.remove(p);
	}

}
