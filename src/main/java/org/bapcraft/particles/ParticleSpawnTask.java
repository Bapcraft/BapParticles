package org.bapcraft.particles;

import java.util.Collection;
import java.util.Optional;

import org.bapcraft.particles.effects.Effect;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.entity.living.player.Player;

public class ParticleSpawnTask implements Runnable {

	private EffectManager manager;

	private int iterNum = 0;

	public ParticleSpawnTask(EffectManager man) {
		this.manager = man;
	}

	@Override
	public void run() {

		Collection<Player> players = Sponge.getServer().getOnlinePlayers();

		for (Player p : players) {

			if (!p.hasPermission(Perms.USE_PARTICLES)) {
				continue;
			}

			Optional<Effect> eo = this.manager.getPlayerEffect(p);
			if (!eo.isPresent()) {
				continue;
			}

			Effect e = eo.get();
			if (this.iterNum % e.getInterval() == 0) {
				e.apply(p.getLocation());
			}

		}

		this.iterNum++;

	}

}
